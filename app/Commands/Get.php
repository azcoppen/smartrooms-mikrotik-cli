<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

use App\Services\Reader;

class Get extends Command
{
    protected $signature = 'get {--h} {--u} {--p} {--mqtt=true} {sentence}';

    protected $description = 'Gets any specified router configuration data.';

    private function __check ()
    {
      if (! env('ROUTER_HOST') )
      {
        $this->error ("ROUTER_HOST is not set in .env file or can't be read.");
      }

      if (! env('ROUTER_USERNAME') )
      {
        $this->error ("ROUTER_USERNAME is not set in .env file or can't be read.");
      }

      if (! env('ROUTER_PASSWORD') )
      {
        $this->error ("ROUTER_PASSWORD is not set in .env file or can't be read.");
      }
    }

    public function handle()
    {
      $this->__check();
      
      $cmd = (new Reader (
          $this->option ('h') ? $this->option ('h') : env('ROUTER_HOST'),
          $this->option ('u') ? $this->option ('u') : env('ROUTER_USERNAME'),
          $this->option ('p') ? $this->option ('p') : env('ROUTER_PASSWORD'),
          $this->argument ('sentence')
          )
        );

      $this->line ($cmd->get());
    }
}
