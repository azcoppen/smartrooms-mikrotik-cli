<?php

namespace App\Commands;

class Services extends BaseReadCommand
{
    protected $signature = 'services {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/ip/service/print';

    protected $description = "Get the router's active network services.";

    protected $mqtt_signal = 'services';
}
