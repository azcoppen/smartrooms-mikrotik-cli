<?php

namespace App\Commands;

class Resources extends BaseReadCommand
{
    protected $signature = 'resources {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/system/resource/print';

    protected $description = "Get the router's resource data.";

    protected $mqtt_signal = 'resources';

    public $single_object = true;
}
