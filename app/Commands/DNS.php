<?php

namespace App\Commands;

class DNS extends BaseReadCommand
{
    protected $signature = 'dns {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/ip/dns/print';

    protected $description = "Get the router's DNS settings.";

    protected $mqtt_signal = 'dns';

    public $single_object = true;
}
