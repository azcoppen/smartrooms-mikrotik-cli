<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

use App\Services\Reader;
use App\Services\MQTTPublisher;

abstract class BaseReadCommand extends Command
{
  protected $sentence;
  public $output_string;
  public $single_object = false;

  private function __check ()
  {
    if (! env('ROUTER_HOST') )
    {
      $this->line ("ROUTER_HOST is not set in .env file or can't be read.");
      die();
    }

    if (! env('ROUTER_USERNAME') )
    {
      $this->line ("ROUTER_USERNAME is not set in .env file or can't be read.");
      die();
    }

    if (! env('ROUTER_PASSWORD') )
    {
      $this->line ("ROUTER_PASSWORD is not set in .env file or can't be read.");
      die();
    }
  }

  public function handle()
  {
    $this->__check();

    $cmd = (new Reader (
        $this->option ('h') ? $this->option ('h') : env('ROUTER_HOST'),
        $this->option ('u') ? $this->option ('u') : env('ROUTER_USERNAME'),
        $this->option ('p') ? $this->option ('p') : env('ROUTER_PASSWORD'),
        $this->sentence
        )
      );

    $this->output_string = $cmd->get($this->single_object);

    if ( $this->option ('mqtt') && $this->option('mqtt') !== FALSE )
    {
      if ( isset ($this->mqtt_signal) && !empty ($this->mqtt_signal) )
      {
        $signal = (new MQTTPublisher)->send (env('MQTT_TOPIC_PREFIX').'/'.$this->mqtt_signal.'/state', $this->output_string);
      }
    }

    $this->line ($this->output_string);

  }

}
