<?php

namespace App\Commands;

class Trafficflow extends BaseReadCommand
{
    protected $signature = 'flow {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/ip/traffic-flow/print';

    protected $description = "Get the router's traffic flow.";

    protected $mqtt_signal = 'flow';
}
