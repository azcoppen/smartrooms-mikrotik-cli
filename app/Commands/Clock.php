<?php

namespace App\Commands;

class Clock extends BaseReadCommand
{
    protected $signature = 'clock {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/system/clock/print';

    protected $description = "Get the router's clock settings.";

    protected $mqtt_signal = 'clock';

    public $single_object = true;
}
