<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

use App\Services\WifiRenamer;
use App\Services\MQTTPublisher;

class Rename extends Command
{
    protected $signature = 'rename {--h} {--u} {--p} {--mqtt=true} {old_name?} {new_name?}';

    protected $description = 'Renames a wireless network.';

    protected $mqtt_signal = 'wifi-renames';

    private function __check ()
    {
      if (! env('ROUTER_HOST') )
      {
        $this->line ("ROUTER_HOST is not set in .env file or can't be read.");
        die();
      }

      if (! env('ROUTER_USERNAME') )
      {
        $this->line ("ROUTER_USERNAME is not set in .env file or can't be read.");
        die();
      }

      if (! env('ROUTER_PASSWORD') )
      {
        $this->line ("ROUTER_PASSWORD is not set in .env file or can't be read.");
        die();
      }
    }

    public function handle()
    {
      $this->__check();

      if (! $this->argument ('old_name') && !$this->argument ('new_name') )
      {
          $this->line('No old name or new name arguments given');
          return;
      }

      if (! preg_match('/^[\w-]+$/', $this->argument ('old_name')) )
      {
        $this->line('Old name can only contain letters, numbers, underscores, and dashes.');
        die();
      }

      if (! preg_match('/^[\w-]+$/', $this->argument ('new_name')) )
      {
        $this->line('New name can only contain letters, numbers, underscores, and dashes.');
        die();
      }

      if ( strlen ($this->argument ('old_name')) < 5 || strlen ($this->argument ('old_name')) > 20 )
      {
        $this->line('Old name must be 5-20 characters.');
      }

      if ( strlen ($this->argument ('new_name')) < 5 || strlen ($this->argument ('new_name')) > 20 )
      {
        $this->line('New name must 5-20 characters.');
      }

      $cmd = (new WifiRenamer (
          $this->option ('h') ? $this->option ('h') : env('ROUTER_HOST'),
          $this->option ('u') ? $this->option ('u') : env('ROUTER_USERNAME'),
          $this->option ('p') ? $this->option ('p') : env('ROUTER_PASSWORD'),
          $this->argument ('old_name'),
          $this->argument ('new_name')
          )
        );

        $this->output_string = $cmd->execute();

        if ( $this->option ('mqtt') && $this->option('mqtt') !== FALSE )
        {
          if ( isset ($this->mqtt_signal) && !empty ($this->mqtt_signal) )
          {
            $signal = (new MQTTPublisher)->send (env('MQTT_TOPIC_PREFIX').'/'.$this->mqtt_signal.'/state', $this->output_string);
          }
        }

        $this->line ($this->output_string);
    }
}
