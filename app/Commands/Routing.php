<?php

namespace App\Commands;

class Routing extends BaseReadCommand
{
    protected $signature = 'routing {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/ip/route/print';

    protected $description = "Get the router's routing settings.";

    protected $mqtt_signal = 'routing';
}
