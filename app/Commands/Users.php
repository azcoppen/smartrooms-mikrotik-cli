<?php

namespace App\Commands;

class Users extends BaseReadCommand
{
    protected $signature = 'users {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/user/print';

    protected $description = "Get the router's user accounts.";

    protected $mqtt_signal = 'users';
}
