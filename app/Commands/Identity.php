<?php

namespace App\Commands;

class Identity extends BaseReadCommand
{
    protected $signature = 'identity {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/system/identity/print';

    protected $description = "Get the router's network hostname.";

    protected $mqtt_signal = 'identity';

    public $single_object = true;
}
