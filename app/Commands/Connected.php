<?php

namespace App\Commands;

class Connected extends BaseReadCommand
{
    protected $signature = 'connected {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/ip/dhcp-server/lease/print';

    protected $description = "Get the devices connected to the router.";

    protected $mqtt_signal = 'connected';
}
