<?php

namespace App\Commands;

class Interfaces extends BaseReadCommand
{
    protected $signature = 'interfaces {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/interface/print';

    protected $description = "Get the router's network interfaces.";

    protected $mqtt_signal = 'interfaces';
}
