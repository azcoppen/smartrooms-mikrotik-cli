<?php

namespace App\Commands;

class DHCP extends BaseReadCommand
{
    protected $signature = 'dhcp {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/ip/dhcp-client/print';

    protected $description = "Get the router's DHCP data.";

    protected $mqtt_signal = 'dhcp';
}
