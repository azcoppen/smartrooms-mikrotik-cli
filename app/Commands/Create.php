<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

use App\Services\WifiCreator;
use App\Services\MQTTPublisher;

class Create extends Command
{
    protected $signature = 'create {--h} {--u} {--p} {--mqtt=true} {name?} {password?}';

    protected $description = 'Creates a new wireless network.';

    protected $mqtt_signal = 'wifi-launches';

    private function __check ()
    {
      if (! env('ROUTER_HOST') )
      {
        $this->line ("ROUTER_HOST is not set in .env file or can't be read.");
        die();
      }

      if (! env('ROUTER_USERNAME') )
      {
        $this->line ("ROUTER_USERNAME is not set in .env file or can't be read.");
        die();
      }

      if (! env('ROUTER_PASSWORD') )
      {
        $this->line ("ROUTER_PASSWORD is not set in .env file or can't be read.");
        die();
      }
    }

    public function handle()
    {
      if (! $this->argument ('name') && !$this->argument ('password') )
      {
          $this->line('No SSID or password arguments given');
          die();
      }

      $this->__check();

      if (! preg_match('/^[\w-]+$/', $this->argument ('name')) )
      {
        $this->line('Name can only contain letters, numbers, underscores, and dashes.');
        die();
      }

      if ( strlen ($this->argument ('name')) < 5 || strlen ($this->argument ('name')) > 20 )
      {
        $this->line('Name must be 5-20 characters.');
      }

      if ( strlen ($this->argument ('password')) < 8 || strlen ($this->argument ('password')) > 20 )
      {
        $this->line('Password must be 8-20 characters.');
        die();
      }

      $cmd = (new WifiCreator (
          $this->option ('h') ? $this->option ('h') : env('ROUTER_HOST'),
          $this->option ('u') ? $this->option ('u') : env('ROUTER_USERNAME'),
          $this->option ('p') ? $this->option ('p') : env('ROUTER_PASSWORD'),
          $this->argument ('name'),
          $this->argument ('password')
          )
        );

      $this->output_string = $cmd->execute();

      if ( $this->option ('mqtt') && $this->option('mqtt') !== FALSE )
      {
        if ( isset ($this->mqtt_signal) && !empty ($this->mqtt_signal) )
        {
          $signal = (new MQTTPublisher)->send (env('MQTT_TOPIC_PREFIX').'/'.$this->mqtt_signal.'/state', $this->output_string);
        }
      }

      $this->line ($this->output_string);
    }
}
