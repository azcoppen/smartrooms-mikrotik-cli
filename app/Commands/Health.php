<?php

namespace App\Commands;

class Health extends BaseReadCommand
{
    protected $signature = 'health {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/system/health/print';

    protected $description = "Get the router's health.";
}
