<?php

namespace App\Commands;

class License extends BaseReadCommand
{
    protected $signature = 'license {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/system/license/print';

    protected $description = "Get the router's license number.";

    protected $mqtt_signal = 'license';

    public $single_object = true;
}
