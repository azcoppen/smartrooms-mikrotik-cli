<?php

namespace App\Commands;

class Proxies extends BaseReadCommand
{
    protected $signature = 'proxies {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/ip/proxy/print';

    protected $description = "Get the router's proxy settings.";

    protected $mqtt_signal = 'proxies';

    public $single_object = true;
}
