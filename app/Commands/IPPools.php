<?php

namespace App\Commands;

class IPPools extends BaseReadCommand
{
    protected $signature = 'ip-pools {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/ip/pool/print';

    protected $description = "Get the router's IP pools.";

    protected $mqtt_signal = 'ip-pools';
}
