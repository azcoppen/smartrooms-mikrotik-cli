<?php

namespace App\Commands;

class Wifi extends BaseReadCommand
{
    protected $signature = 'wifi {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/interface/wireless/print';

    protected $description = "Get the router's wireless networks.";

    protected $mqtt_signal = 'wlans';
}
