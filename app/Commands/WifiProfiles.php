<?php

namespace App\Commands;

class WifiProfiles extends BaseReadCommand
{
    protected $signature = 'wifi-profiles {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/interface/wireless/security-profiles/print';

    protected $description = "Get the router's wireless security profiles.";

    protected $mqtt_signal = 'secprofiles';
}
