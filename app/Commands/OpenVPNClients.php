<?php

namespace App\Commands;

class OpenVPNClients extends BaseReadCommand
{
    protected $signature = 'ovpn-clients {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/interface/ovpn-client/print';

    protected $description = "Get the router's OpenVPN client connections.";

    protected $mqtt_signal = 'vpns';
}
