<?php

namespace App\Commands;

class Routerboard extends BaseReadCommand
{
    protected $signature = 'routerboard {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/system/routerboard/print';

    protected $description = "Get the router's routerboard info.";

    protected $mqtt_signal = 'routerboard';

    public $single_object = true;
}
