<?php

namespace App\Commands;

class IPAddresses extends BaseReadCommand
{
    protected $signature = 'ip-addresses {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/ip/address/print';

    protected $description = "Get the router's IP addresses.";

    protected $mqtt_signal = 'ip-addresses';
}
