<?php

namespace App\Commands;

class Ethernet extends BaseReadCommand
{
    protected $signature = 'ethernet {--h} {--u} {--p} {--mqtt=true}';

    protected $sentence = '/interface/ethernet/print';

    protected $description = "Get the router's ethernet networks.";

    protected $mqtt_signal = 'ethernet';
}
