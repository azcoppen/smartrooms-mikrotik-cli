<?php

namespace App\Services;

use Salman\Mqtt\MqttClass\Mqtt;
use \Exception;

class MQTTPublisher
{
  private function __check ()
  {
    if ( env ('MQTT_HOST') == 'localhost' || env ('MQTT_HOST') == '127.0.0.1' )
    {
      config(['mqtt.certfile' => '']);
      config(['mqtt.port' => '1883']);
    }

    if ( !env ('MQTT_HOST') )
    {
      throw new Exception ("MQTT_HOST is not set in .env file.");
    }

    if ( !env ('MQTT_USERNAME') )
    {
      throw new Exception ("MQTT_USERNAME is not set in .env file.");
    }

    if ( !env ('MQTT_PASSWORD') )
    {
      throw new Exception ("MQTT_PASSWORD is not set in .env file.");
    }

    if ( !env ('MQTT_TOPIC_PREFIX') )
    {
      throw new Exception ("MQTT_TOPIC_PREFIX is not set in .env file.");
    }

    if ( !env ('MQTT_CLIENT_ID') )
    {
      throw new Exception ("MQTT_CLIENT_ID (HAB id) is not set in .env file.");
    }

    if ( !env ('MQTT_CERT_FILE') )
    {
      throw new Exception ("MQTT_CERT_FILE is not set in .env file and MUST be an ABSOLUTE filesystem path (or relative to the OpenHAB process).");
    }

    if ((env ('MQTT_HOST') == 'localhost' || env ('MQTT_HOST') == '127.0.0.1') && ! file_exists (config ('mqtt.certfile')) )
    {
      throw new Exception ("CA certificate for TLS connection needs to be in this directory or relative to the host process.");
    }
  }

  public function send ( string $topic, $payload ) : bool
  {
    $this->__check();

    return (new Mqtt())->ConnectAndPublish ($topic, $payload, env('MQTT_CLIENT_ID'));
  }

}
