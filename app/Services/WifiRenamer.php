<?php

namespace App\Services;

use \RouterOS\Client;
use \RouterOS\Query;
use RouterOS\Exceptions\ClientException;

use Illuminate\Support\Str;
use \Exception;

class WifiRenamer
{
  public $host;
  public $user;
  public $pass;
  public $old_ssid;
  public $new_ssid;
  public $client;

  public function __construct (string $host, string $user, string $pass, string $old_ssid, string $new_ssid)
  {
    $this->host = $host;
    $this->user = $user;
    $this->pass = $pass;

    $this->old_ssid = $old_ssid;
    $this->new_ssid = preg_replace('/[^\w-]/', '', $new_ssid); // remove anything but alphanumeric, underscore, dash
  }

  public function execute () : string
  {
    try
    {

      $this->client = new Client ([
          'host' => $this->host,
          'user' => $this->user,
          'pass' => $this->pass
      ]);

      // First let's check if we have any wifi networks with the old name.

      $networks = collect($this->client->query(
        (new Query('/interface/wireless/print'))
      )->read());

      if (!$networks->where('ssid', $this->old_ssid)->count() )
      {
        throw new Exception ("Network doesn't exist.");
      }

      // Next, let's check we don't have any with the new name.
      if ($networks->where('ssid', $this->new_ssid)->count() )
      {
        $this->new_ssid .= "-".uniqId();
      }

      $old_network = $networks->where('ssid', $this->old_ssid)->first();

      // Finally, let's change the network itself.
      $ssid_query = (new Query ('/interface/wireless/set'))
        ->equal ('.id', $old_network['.id'])
        ->equal ('ssid', $this->new_ssid);

      $ssid_response = $this->client->query ($ssid_query)->read();

      if ( $ssid_response && is_array ($ssid_response) )
      {
          if ( array_key_exists ('after', $ssid_response) )
          {
              if (array_key_exists ('message', $ssid_response['after']) )
              {
                  throw new Exception ($ssid_response['after']['message']);
              }

              if (array_key_exists ('ret', $ssid_response['after']) )
              {
                  $this->network_id = $ssid_response['after']['ret'];
              }
          }
      }

      // Finally let's check the network is up and send back the info

      $networks = collect($this->client->query(
        (new Query('/interface/wireless/print'))
      )->read());

      return collect ([
        $old_network['.id'],
        $this->old_ssid,
        $this->new_ssid,
      ])->implode(',');

    }
    catch (ClientException $e)
    {
      return $e->getMessage();
    }
    catch (Exception $e)
    {
      return $e->getMessage();
    }

    return '';
  }


}
