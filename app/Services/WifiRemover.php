<?php

namespace App\Services;

use \RouterOS\Client;
use \RouterOS\Query;
use RouterOS\Exceptions\ClientException;

use Illuminate\Support\Str;
use \Exception;

class WifiRemover
{
  public $host;
  public $user;
  public $pass;
  public $ssid;
  public $client;

  public function __construct (string $host, string $user, string $pass, string $ssid)
  {
    $this->host = $host;
    $this->user = $user;
    $this->pass = $pass;

    $this->ssid = $ssid;
  }

  public function execute () : string
  {
    try
    {

      $this->client = new Client ([
          'host' => $this->host,
          'user' => $this->user,
          'pass' => $this->pass
      ]);

      // First let's check if we have any wifi networks with this name.

      $networks = collect($this->client->query(
        (new Query('/interface/wireless/print'))
      )->read());

      if (!$networks->where('ssid', $this->ssid)->count() )
      {
        throw new Exception ("Network doesn't exist.");
      }

      $network = $networks->where('ssid', $this->ssid)->first();

      // Let's get the security profile
      $profiles = collect($this->client->query(
        (new Query('/interface/wireless/security-profiles/print'))
      )->read());

      $profile = $profiles->where('name', $network['security-profile'])->first();

      // First let's kill the network, as one defpends on another
      $network_query = (new Query ('/interface/wireless/remove'))
        ->equal ('.id', $network['.id']);

      $network_response = $this->client->query ($network_query)->read();

      // Finally, let's kill the security profile
      $profile_query = (new Query ('/interface/wireless/security-profiles/remove'))
        ->equal ('.id', $profile['.id']);

      $profile_response = $this->client->query ($profile_query)->read();

      return collect ([
        $network['.id'],
        $this->ssid,
        $network['.id'],
        $network['security-profile'],
      ])->implode (',');

    }
    catch (ClientException $e)
    {
      return $e->getMessage();
    }
    catch (Exception $e)
    {
      return $e->getMessage();
    }

    return '';
  }


}
