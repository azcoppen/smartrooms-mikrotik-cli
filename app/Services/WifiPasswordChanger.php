<?php

namespace App\Services;

use \RouterOS\Client;
use \RouterOS\Query;
use RouterOS\Exceptions\ClientException;

use Illuminate\Support\Str;
use \Exception;

class WifiPasswordChanger
{
  public $host;
  public $user;
  public $pass;
  public $ssid;
  public $new_password;
  public $client;

  public function __construct (string $host, string $user, string $pass, string $ssid, string $new_password)
  {
    $this->host = $host;
    $this->user = $user;
    $this->pass = $pass;

    $this->ssid = $ssid;
    $this->new_password = $new_password;
  }

  public function execute () : string
  {
    try
    {

      $this->client = new Client ([
          'host' => $this->host,
          'user' => $this->user,
          'pass' => $this->pass
      ]);

      // First let's check if we have any wifi networks with this name.

      $networks = collect($this->client->query(
        (new Query('/interface/wireless/print'))
      )->read());

      if (!$networks->where('ssid', $this->ssid)->count() )
      {
        throw new Exception ("Network doesn't exist.");
      }

      $network = $networks->where('ssid', $this->ssid)->first();

      // Let's get the security profile
      $profiles = collect($this->client->query(
        (new Query('/interface/wireless/security-profiles/print'))
      )->read());

      $profile = $profiles->where('name', $network['security-profile'])->first();

      // Finally, let's change the profile itself with the new password
      $profile_query = (new Query ('/interface/wireless/security-profiles/set'))
        ->equal ('.id', $profile['.id'])
        ->equal ('wpa-pre-shared-key', $this->new_password)
        ->equal ('wpa2-pre-shared-key', $this->new_password);

      $profile_response = $this->client->query ($profile_query)->read();

      if ( $profile_response && is_array ($profile_response) )
      {
          if ( array_key_exists ('after', $profile_response) )
          {
              if (array_key_exists ('message', $profile_response['after']) )
              {
                  throw new Exception ($profile_response['after']['message']);
              }

              if (array_key_exists ('ret', $profile_response['after']) )
              {
                  $this->network_id = $profile_response['after']['ret'];
              }
          }
      }

      return collect ([
        $network['.id'],
        $this->ssid,
        $this->new_password,
        $profile['.id'],
        $network['security-profile'],
      ])->implode (',');

      return $ssid.' password changed to '.$new_password;

    }
    catch (ClientException $e)
    {
      return $e->getMessage();
    }
    catch (Exception $e)
    {
      return $e->getMessage();
    }

    return '';
  }


}
