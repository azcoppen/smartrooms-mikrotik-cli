<?php

namespace App\Services;

use \RouterOS\Client;
use \RouterOS\Query;
use RouterOS\Exceptions\ClientException;

use Illuminate\Support\Str;
use \Exception;

class WifiCreator
{
  public $host;
  public $user;
  public $pass;
  public $profile;
  public $ssid;
  public $password;
  public $client;
  public $profile_id;
  public $network_id;

  public function __construct (string $host, string $user, string $pass, string $ssid, string $password)
  {
    $this->host = $host;
    $this->user = $user;
    $this->pass = $pass;
    $this->ssid = preg_replace('/[^\w-]/', '', $ssid); // remove anything but alphanumeric, underscore, dash
    $this->password = $password;
  }

  public function execute () : string
  {
    try
    {

      $this->client = new Client ([
          'host' => $this->host,
          'user' => $this->user,
          'pass' => $this->pass
      ]);

      // First let's check if we have any wifi networks with the same name.
      // if we do, add a uniqId to avoid the clash.

      $networks = collect($this->client->query(
        (new Query('/interface/wireless/print'))
      )->read());

      if ($networks->where('ssid', $this->ssid)->count() )
      {
        $this->ssid .= "-".uniqId();
      }

      // Next let's create a new security profile for this network.
      // set to a random name to avoid clashes
      $this->profile_name = uniqId();

      $profile_query = (new Query ('/interface/wireless/security-profiles/add'))
        ->equal ('mode', 'dynamic-keys')
        ->equal ('authentication-types', 'wpa2-psk')
        ->equal ('name', $this->profile_name)
        ->equal ('wpa-pre-shared-key', $this->password)
        ->equal ('wpa2-pre-shared-key', $this->password);

      $profile_response = $this->client->query ($profile_query)->read();

      if ( $profile_response && is_array ($profile_response) )
      {
          if ( array_key_exists ('after', $profile_response) )
          {
              if (array_key_exists ('message', $profile_response['after']) )
              {
                  throw new Exception ($profile_response['after']['message']);
              }

              if (array_key_exists ('ret', $profile_response['after']) )
              {
                  $this->profile_id = $profile_response['after']['ret'];
              }
          }
      }

      // Finally, let's create the network itself.
      $ssid_query = (new Query ('/interface/wireless/add'))
        ->equal ('mode', 'ap-bridge')
        ->equal ('disabled', 'no')
        ->equal ('master-interface', 'wlan1')
        ->equal ('security-profile', $this->profile_name)
        ->equal ('ssid', $this->ssid);

      $ssid_response = $this->client->query ($ssid_query)->read();

      if ( $ssid_response && is_array ($ssid_response) )
      {
          if ( array_key_exists ('after', $ssid_response) )
          {
              if (array_key_exists ('message', $ssid_response['after']) )
              {
                  throw new Exception ($ssid_response['after']['message']);
              }

              if (array_key_exists ('ret', $ssid_response['after']) )
              {
                  $this->network_id = $ssid_response['after']['ret'];
              }
          }
      }

      // Finally let's check the network is up and send back the info

      $networks = collect($this->client->query(
        (new Query('/interface/wireless/print'))
      )->read());

      return collect ([
        $this->network_id,
        $this->ssid,
        $this->password,
        $this->profile_id,
        $this->profile_name,
      ])->implode (',');

    }
    catch (ClientException $e)
    {
      return $e->getMessage();
    }
    catch (Exception $e)
    {
      return $e->getMessage();
    }

    return '';
  }


}
