<?php

namespace App\Services;

use \RouterOS\Client;
use \RouterOS\Query;
use RouterOS\Exceptions\ClientException;

class Reader
{
  public $host;
  public $user;
  public $pass;
  public $sentence;
  public $client;

  public function __construct (string $host, string $user, string $pass, string $sentence)
  {
    $this->host = $host;
    $this->user = $user;
    $this->pass = $pass;
    $this->sentence = $sentence;
  }

  public function get ( bool $is_single_object = false ) : string
  {
    try
    {

      $this->client = new Client ([
          'host' => $this->host,
          'user' => $this->user,
          'pass' => $this->pass
      ]);

      $this->response = $this->client->query(
        (new Query($this->sentence))
      )->read();

      if ( $is_single_object != FALSE )
      {
        $this->response = collect ($this->response)->first();
      }

      return json_encode ($this->response, JSON_HEX_QUOT|JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS|JSON_NUMERIC_CHECK);
    }
    catch (ClientException $e)
    {
      return $e->getMessage();
    }
    catch (Exception $e)
    {
      return $e->getMessage();
    }

    return '';
  }


}
